#### 介绍
本项目收集了互联网各个大厂面试题，包括前端、后端、测试等，方便大家回顾和学习总结、找到心仪的Offer。

如果觉得本项目还可以，欢迎 **Star**分享哦，博主在此谢谢大家啦。


这里推荐一个优秀的公众号【黑科技程序员】，一个干货-资源分享的公众号，这个公众号专注于写干货资源分享、技术类文章，欢迎广大用户扫一扫关注一波，我想你一定会有所收获。

![image](./黑科技程序员.jpg)




> 目录

- [前端](#前端)
- [后端](#后端)
- [面经](#面经)
- [声明](#声明)
# 前端
- 猪八戒2018秋招前端试题   [资源下载链接](https://hkjcxy.lanzous.com/iiCZohivcgj)密码:1fa0
- 艺龙2018秋招前端试题       [资源下载链接](https://hkjcxy.lanzous.com/iLfRuhivcfi)密码:a2fz
- 腾讯2018秋招前端正式试题  [资源下载链接](https://hkjcxy.lanzous.com/i01zthivceh)密码:favt
- 腾讯2018秋招前端模拟题      [资源下载链接](https://hkjcxy.lanzous.com/iKj8Rhivcdg)密码:5eh1
- 搜狗2018秋招前端试题          [资源下载链接](https://hkjcxy.lanzous.com/iUQp9hivcbe)密码:cqet
- 美团点评2018秋招前端测评    [资源下载链接](https://hkjcxy.lanzous.com/i5t2Rhivcad)密码:b2dc
- 饿了么2018秋招笔试题前端试题  [资源下载链接](https://hkjcxy.lanzous.com/iC8fQhivc7a)密码:5gb4
- bilibili2018前端秋招试题    [资源下载链接](https://hkjcxy.lanzous.com/i0nAvhivc6j)密码:48v0
- 2020华为前端开发面试      [资源下载链接](https://hkjcxy.lanzous.com/iS23hhivc5i)密码:1iy7
- 2019年小米前端工程师面试题及答案  [资源下载链接](https://hkjcxy.lanzous.com/iXHM8hivc4h)密码:2v4d
- 2019年网易前端工程师面试题及答案   [资源下载链接](https://hkjcxy.lanzous.com/iQA43hivc3g)密码:g4um
- 2019年腾讯前端工程师面试题及答案   [资源下载链接](https://hkjcxy.lanzous.com/i6UbQhivc2f)密码:2zus
- 2019年美团前端面试题   [资源下载链接](https://hkjcxy.lanzous.com/ifLyXhivc1e)密码:ev5n
- 2019年京东前端工程师面试题及答案  [资源下载链接](https://hkjcxy.lanzous.com/iMAfchivc0d)密码:eppt
- 2019年滴滴出行前端工程师笔试题及答案   [资源下载链接](https://hkjcxy.lanzous.com/i06Eahivbzc)密码:3qn1
- 2019年百度前端工程师面试题及答案  [资源下载链接](https://hkjcxy.lanzous.com/iZ5Tmhivbyb)密码:63k5
- 2019年阿里前端工程师面试题及答案   [资源下载链接](https://hkjcxy.lanzous.com/ibgZphivbxa)密码:e9wr
- 2018秋招链家前端笔试题  [资源下载链接](https://hkjcxy.lanzous.com/ikk0nhivbvi)密码:79bg
- 58同城2018秋招前端笔试试题 [资源下载链接](https://hkjcxy.lanzous.com/ilXdchih0jg) 密码:4kmj


# 后端
- 2018秋招freewheeljava笔试题     [资源下载链接](https://hkjcxy.lanzous.com/iaABPhivp3e)密码:b6dd
- 2018秋招阿里巴巴java笔试试题    [资源下载链接](https://hkjcxy.lanzous.com/iVTW1hivp4f)密码:6u55
- 2018秋招笔试科大讯飞java笔试试题  [资源下载链接](https://hkjcxy.lanzous.com/i9fIlhivp5g)密码:6w62
- 2019美团JAVA工程师面试题   [资源下载链接](https://hkjcxy.lanzous.com/i3YCchivp6h)密码:73kc
- 2019年阿里JAVA工程师笔试题及答案 [资源下载链接](https://hkjcxy.lanzous.com/igoe2hivp7i)密码:dguo
- 2019年阿里巴巴校招JAVA面试题   [资源下载链接](https://hkjcxy.lanzous.com/icJ5Rhivp8j)密码:emdg
- 2019年百度JAVA工程师笔试题及答案  [资源下载链接](https://hkjcxy.lanzous.com/iPBfhhivp9a)密码:8ab7
- 2019年华为校招JAVA面试题  [资源下载链接](https://hkjcxy.lanzous.com/i45I3hivpab)密码:c7wk
- 2019年腾讯JAVA工程师面试题及答案 [资源下载链接](https://hkjcxy.lanzous.com/iOn5Nhivpde)密码:6pb4
- 2019年腾讯校招JAVA面试题   [资源下载链接](https://hkjcxy.lanzous.com/iyMKShivpef)密码:8uny
- eBay2018秋招Java试题  [资源下载链接](https://hkjcxy.lanzous.com/iTqK9hivpfg)密码:6qda
- 酷狗java2018秋招笔试题目  [资源下载链接](https://hkjcxy.lanzous.com/iE3p3hivpgh)密码:4qb0
- 联想2018秋招java笔试题目  [资源下载链接](https://hkjcxy.lanzous.com/id3ykhivphi)密码:2r2h
- 链家2018秋招Java笔试题    [资源下载链接](https://hkjcxy.lanzous.com/im50Hhivpij)密码:67e0
- 美图2018秋招Java笔试题 [资源下载链接](https://hkjcxy.lanzous.com/i9kK4hivpja)密码:3ity
- 盛大游戏2018秋招Java笔试题  [资源下载链接](https://hkjcxy.lanzous.com/ie2vDhivpkb)密码:8xr8
- 搜狗2018秋招Java笔试题    [资源下载链接](https://hkjcxy.lanzous.com/iOw8zhivplc)密码:3nj5
- 搜狗2018秋招Java客户端试题  [资源下载链接](https://hkjcxy.lanzous.com/iy40Yhivpmd)密码:55zf
- 腾讯2018秋招Java模拟题    [资源下载链接](https://hkjcxy.lanzous.com/isIe3hivpne)密码:18yb
- 网龙2018秋招Java笔试题    [资源下载链接](https://hkjcxy.lanzous.com/iBFp2hivpof)密码:2v2s
- 艺龙2018秋招Java笔试题    [资源下载链接](https://hkjcxy.lanzous.com/i3yhyhivppg)密码:828r
- 猪八戒2018秋招Java笔试    [资源下载链接](https://hkjcxy.lanzous.com/imTS4hivpqh)密码:f7ks

# 面经
- 阿里巴巴      [资源下载链接](https://hkjcxy.lanzous.com/iMSnIhiw61e)密码:bkad
- 百度             [资源下载链接](https://hkjcxy.lanzous.com/ibIkxhiw62f)密码:d8ef
- 今日头条      [资源下载链接](https://hkjcxy.lanzous.com/iltRchiw63g)密码:25m6
- 美团              [资源下载链接](https://hkjcxy.lanzous.com/ijtbEhiw64h)密码:7d2h
- 搜狗              [资源下载链接](https://hkjcxy.lanzous.com/i5dJPhiw65i)密码:cuc1
- 腾讯              [资源下载链接](https://hkjcxy.lanzous.com/i5IfYhiw66j)密码:4gnb
- 网易              [资源下载链接](https://hkjcxy.lanzous.com/ic66Thiw67a)密码:ao57
- 小米               [资源下载链接](https://hkjcxy.lanzous.com/iRMZRhiw68b)密码:7xxr
- 字节跳动       [资源下载链接](https://hkjcxy.lanzous.com/ibAOPhiw69c)密码:cxwq

# 声明
本项目整理了互联网大厂面试题，方便大家学习交流，如有侵权，马上联系我，我立马删除对应链接。我的邮箱：coderfeng@163.com
